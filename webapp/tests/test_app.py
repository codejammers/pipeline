import os
import pytest
import requests

web_ip = os.environ.get('WEB_IP', 'localhost')
web_port = os.environ.get('WEB_PORT', '8888')
url = "http://{0}:{1}/add/".format(web_ip, web_port)

def test_addition():
  myobj = { "num1": 10, "num2": 10 }
  x = requests.post(url, json=myobj, verify=False)
  result = int(x.text)
  assert result == 20, "Addition is failed"
