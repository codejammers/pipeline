import json
from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop

class add(RequestHandler):
    def post(self):
        mydict = json.loads(self.request.body)
        num1 = int(mydict.get("num1"))
        num2 = int(mydict.get("num2"))
        total = str(num1 + num2)
        self.write(total)

application = Application([
    (r"/add/", add),
])

if __name__ == "__main__":
    application.listen(8888)
    IOLoop.instance().start()
